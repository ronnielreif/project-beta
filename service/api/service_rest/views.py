from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'id',
        'vin',
        'sold',
        'href',
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]
    encoders = {
        'technician': TechnicianEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.create(**content)
        return JsonResponse(
            technicians,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST", "PUT"])
def api_list_appointments(request, vin=None):
    if request.method == "GET":
        if vin:
            appointment = Appointment.objects.filter(vin=vin)
        else:
            appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        appointment_vin = vin
        print(vin)
        try:
            appointment = Appointment.objects.get(vin=appointment_vin)
            for key , value in content.items():
                setattr(appointment, key, value)
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
                )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment with specified VIN not found."}, status=404)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
