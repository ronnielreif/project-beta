# CarCar

# Team:

* Stetson - Sales
* Ronnie - Service

## How to Run this App

Use these docker commands in terminal:

    docker volume create beta-data
    docker-compose build
    docker-compose up

Make sure its running in Docker.

View project in browser here: http://localhost:3000/

## Diagram
<!-- alt click png -->
<img src="Diagram_Project_Beta.png">

### URLs, PORTS, and APIs

Homepage: http://localhost:3000

sale servives:
                http://localhost:3000/allsales/
                <!-- lists all sales made. -->
                http://localhost:3000/sales/
                <!-- lists sales made by specified salesperson -->
                http://localhost:3000/sales/create/
                <!-- creates a sale -->
                http://localhost:3000/customer/create/
                <!-- creates a customer -->
                http://localhost:3000/salesperson/create/
                <!-- creates a salesperson -->
APIs
http://localhost:8090/api/sales/
<!-- accepts "GET" and "POST" should be filled out as such
{
    "price": "10000",
    "automobile": "1234567890",
    "salesperson": "1",
    "customer": "1"
}
"automobile" needs to be the vin of an automobile in inventory and "salesperson/customer" need to be the id of the salesperson/customer
-->
http://localhost:8090/api/customers/
<!-- accepts "GET" and "POST" should be filled out as such
{
    "first_name": "mary",
    "last_name": "doe",
    "address": "1111 11th lane",
    "phone_number": "000-000-0000"
}
-->
http://localhost:8090/api/salespeople/
<!-- accepts "GET and "POST" should be filled out as such
{
    "first_name": "john",
    "last_name": "doe",
    "employee_id": "jdoe"
}
-->
Services:  port:8080
    Technincians:
        http://localhost:8080/api/technicians/
            Used to view the current technicians

        http://localhost:8080/api/technicians/create/
            Contains a form that can create a technician

            JSON input:
                {
                    "first_name": "Test",
                    "last_name": "Testerson",
                    "employee_id": "987456"
                }


    Appointment/Service:
        http://localhost:8080/api/appointments/
            Used to view appointments that have been created and have
            not been marked finished or canceled
            Has two buttons that can change a appointment from "Created" to "Finished" or "Canceled"
            Contains a search bar that will search the list for an appointment with match vin.

        http://localhost:8080/api/appointments/create/
            Contains a form that can create a appointment.
            Vin number is used as the identifier and to look at specific appointments
            in URL path. Example :http://localhost:8080/api/appointments/3333333333/
            JSON input:
            {
                "date_time": "2012-04-23T18:25:43.511Z",
                "reason": "season",
                "vin": "3333333333",
                "customer": "ssdfan",
                "technician_id": "1"
            }

        http://localhost:8080/api/appointments/history/
            Used to view all appointments.
            includes: created, finished, and canceld appointments

Inventory: port:8100
    Manufacturers:
        http://localhost:8100/api/manufacturers/
        Shows list of manufacturers

        http://localhost:8100/api/manufacturers/create/
        contains form to create a manufacturer
        JSON input:
            {
                "name": "Mazda"
            }

    Models:
        http://localhost:8100/api/models/
        shows list of models

        http://localhost:8100/api/models/create/
        Contains form that creates a model
        JSON input:
            {
                "name": "Mazda",
                "picture_url": "{picture_url_here}",
                "manufacturer_id": 1
            }

    Automobiles:
        http://localhost:8100/api/automobiles/
        shows a list of automobiles

        http://localhost:8100/api/automobiles/create/
        Contains a form that can create an automobile
        Vin number is used as the identifier and to look at specific appointments
        in URL path. Example :http://localhost:8080/api/appointments/1C12312342374/
        JSON input:
            {
                "color": "brown",
                "year": 2012,
                "vin": "1C12312342374",
                "model_id": 2,
            }


## Value Objects
AutomobileVO:
The Value Objects for this application is AutomobileVO. This gets the automobile data
from the inventory models' Automobile model
