import React, { useState, useEffect } from 'react';

function AppointmentHistory({ appointment, autos }) {
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredAppointments, setFilteredAppointments] = useState(appointment);

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleSearch = () => {
    const filtered = appointment.filter(app => app.vin.startsWith(searchQuery));
    setFilteredAppointments(filtered);
  };

  useEffect(() => {
    setFilteredAppointments(appointment ? appointment : []);
  }, [appointment]);

  const checkVin = (appointmentVin) => {
    return autos.some(auto => auto.vin === appointmentVin) ? 'Yes' : 'No';
  };

  return (
    <div>
        <input
            type="text"
            placeholder="Search by VIN"
            value={searchQuery}
            onChange={handleSearchChange}/>
        <button onClick={handleSearch}>Search</button>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map(appointment => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{checkVin(appointment.vin)}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString('en-US', {
                month: '2-digit',
                day: '2-digit',
                year: 'numeric'
              })}</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString('en-US', {
                hour: 'numeric',
                minute: '2-digit',
                second: '2-digit',
                hour12: true
              })}</td>
              <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentHistory;
