import React, {useState} from 'react';

function CustomerForm() {
    const [FormData, setFormData] = useState({
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
    })
    const handleSubmit = async (event) => {
        event.preventDefault();
        const customerUrl = "http://localhost:8090/api/customers/"

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(FormData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(customerUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: "",
                last_name: "",
                address: "",
                phone_number: "",
            })
        }
    }
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...FormData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Customer</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.first_name} placeholder="First_name" required type="text" name="first_name" id="first_name" className="form-control" />
                  <label htmlFor="first_name">First_name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.last_name} placeholder="Last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                  <label htmlFor="last_name">Last_name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFormChange} value={FormData.phone_number} placeholder="Phone_number" required type="text" name="phone_number" id="phone_number" alt="emptystring" className="form-control" />
                  <label htmlFor="phone_number">Phone_number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}
export default CustomerForm;
