import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

          <li className="nav-item dropdown">
              <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Manufacturers
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/manufacturers">View Manufacturers</NavLink></li>
              <li><NavLink className="dropdown-item" to="/manufacturers/create">Add a Manufacturers</NavLink></li>
            </ul>
            </li>

            <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Models
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/models">View Models</NavLink></li>
              <li><NavLink className="dropdown-item" to="/models/create">Add a Model</NavLink></li>
            </ul>
            </li>

            <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Automobiles
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/automobiles">View Automobiles</NavLink></li>
              <li><NavLink className="dropdown-item" to="/automobiles/create">Add a Automobile</NavLink></li>
            </ul>
            </li>

            <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Technicians
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/technicians">View Technicians</NavLink></li>
              <li><NavLink className="dropdown-item" to="/technicians/create">Add a Technician</NavLink></li>
            </ul>
            </li>

            <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Services
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/appointments">View Service Appointments</NavLink></li>
              <li><NavLink className="dropdown-item" to="/appointments/create">Add a Service Appointment</NavLink></li>
              <li><NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink></li>
            </ul>
            </li>

            <li className="nav-item dropdown">
            <button className="nav-link dropdown-toggle btn btn-link" id="navbarDropdown" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </button>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><NavLink className="dropdown-item" to="/allsales">View Sales</NavLink></li>
              <li><NavLink className="dropdown-item" to="/sales">Search Sales</NavLink></li>
              <li><NavLink className="dropdown-item" to="/sales/create">Create Sale</NavLink></li>
            </ul>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/customer/create/">Create customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salesperson/create/">Create salesperson</NavLink>
            </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
