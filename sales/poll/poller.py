import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            responce = requests.get("http://inventory-api:8000/api/automobiles/")
            content = json.loads(responce.content)
            for automobile in content['autos']:
                AutomobileVO.objects.update_or_create(
                    import_href=automobile["href"],
                    vin=automobile["vin"],
                    sold=automobile["sold"],
                )
        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(5)
        # this is set this low becasue it needs to update the cars immediately for the
        # automobile to be used correctly for sales


if __name__ == "__main__":
    poll()
